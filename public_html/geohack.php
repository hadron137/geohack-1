<?PHP

/**
 (c) by Magnus Manske (2006)
 Released under GPL
 geo_param.php is (c) 2005, Egil Kvaleberg <egil@kvaleberg.no>, also GPL
*/

#include "../common.php" ;
ob_start ( 'ob_gzhandler' ) ; # Enable gzip compression (mod_gzip)
ini_set ( 'user_agent', 'Geohack (+http://tools.wmflabs.org/geohack)' ) ; # Set user agent
set_time_limit ( 20 ) ; # 20 sec on region lookup should be enough for everyone!
#ini_set('display_errors',1);
#error_reporting(E_ALL);

include "geo_param.php" ;
include "mapsources.php" ;
#include "region.php" ; 

function get_request ( $key , $default = "" ) {
	if ( isset ( $_REQUEST[$key] ) ) {
		$ret = str_replace ( "\'" , "'" , $_REQUEST[$key] ) ;
		$ret = preg_replace ( '/<script.+<\/script>/' , '' , $ret ) ; # Prevent JS injection
		return $ret ;
	}
	return $default ;
}
function fix_language_code ( $lang , $default = "en" ) {
  $lang = trim ( strtolower ( $lang ) ) ;
  if ( preg_match ( "/^([\-a-z]+)/" , $lang , $l ) ) {
    $lang = $l[0] ;
  } else $lang = $default ; // Fallback
  return $lang ;
}
function get_div_section ($html, $nodeId, $begin = 0) {
	$begin = strpos($html, "<div id=\"".$nodeId."\"", $begin);
	if($begin==false) return '';
	$end=$start=$begin;
	do{
		$end = strpos($html, "</div>", $end+6);
		$start = strpos($html, "<div", $start+4);
	 }while($start!=false && $start < $end );
	 return substr($html, $begin, $end-$begin+6);
}
function make_link ( $lang , $theparams , $r_pagename ) {
	# TODO theparams match characters: %+
	$query = ( $r_pagename ? '&pagename=' . $r_pagename : '' ) ;
	if ( preg_match( '/[^0-9A-Za-z_.:;@$!*(),\/\\-]/', $theparams ) == 0 ) {
		# Short url
		$path = "/geohack/$lang/" . $theparams ;
//		$path = "?" . $theparams . "&language=&$lang" ;
	} else {
		$path = $_SERVER['SCRIPT_NAME'] ;
		$query = "&language=$lang$query&params=$theparams" ;
	}
	if ( isset ( $_REQUEST['title'] ) ) {
		$query .= '&title=' . htmlspecialchars ( $_REQUEST['title'] ) ;
	}
	if ( $query ) {
		return $path . "?" . substr ( $query , 1 ) ;
	} else {
		return $path ;
	}
}
function get_html ( $request_url ) {
	$context = stream_context_create ( array ( "http" => array( "method" => "GET", "header" => "Accept-Encoding: gzip" ) ) ) ;
	$page = file_get_contents ( $request_url, false, $context ) ;
	# ungzip if needed
	if ( $page && substr( $page , 0, 3 ) == "\x1f\x8b\x08" ) {
		return gzinflate ( substr($page, 10, -8)  ) ;
	} else {
		return $page ;
	}
}

# Get everything we need to run
$lang = fix_language_code ( get_request ( 'language' , 'en' ) , '' ) ;
$theparams = htmlspecialchars ( get_request ( 'params' , '' ) ) ;
if ( $theparams == '' ) {
	header("HTTP/1.1 400 Bad Request");
	print "No parameters given (<code>&amp;params=</code> is empty or missing)" ;
	print "<br>Usage: https://www.mediawiki.org/wiki/GeoHack";
	print "<br>Source code: https://bitbucket.org/magnusmanske/geohack";
	exit;
}

# Using REFERER as a last resort for pagename
preg_match("/https?:\/\/[^\/]+\/?(?:wiki\/|w\/index.php\?.*?title=)([^&?#{|}\[\]]+)/", ( array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '' ), $ref_match );
$r_pagename = get_request( 'pagename', ( $ref_match ? urldecode( $ref_match[1] ) : '' ) ) ;
$r_title = get_request( 'title', str_replace( '_', ' ', $r_pagename ) ) ;
$r_pagename = htmlspecialchars ( $r_pagename ) ;
$r_title = htmlspecialchars ( $r_title ) ;

# Initilize Map Sources
$md = new map_sources ( $theparams , "Some title" ) ;

if (($e = $md->p->get_error()) != "") {
	header("HTTP/1.1 400 Bad Request");
	print       "<p>" . htmlspecialchars( $e ) . "</p>";
	exit ;
}

# (auto-)detect region
$region_name = false ;
$globe = '';
$nlzoom = '';
foreach ( $md->p->pieces AS $k => $v ) {
	if ( substr ( $v , 0 , 7 ) == "region:" )
		$region_name = strtoupper ( substr ( $v , 7 ) ) ;
	else if ( substr ( $v , 0 , 6) == "globe:" )
		$globe = strtolower ( substr ( $v , 6 ) );
	else if ( substr ( $v , 0 , 5) == "zoom:" )
		$nlzoom = strtolower ( substr ( $v , 5 ) );
}
$lat = $md->p->make_minsec($md->p->latdeg);
$lon = $md->p->make_minsec($md->p->londeg);
if (1==0 && $region_name === false && ! ( $globe != '' && $globe != 'earth' ) ) {    ## deactivated Kolossos 2016-06-03
	$url = "http://tools.wmflabs.org/para/geo/worldadmin98?tsv&lat=" . $lat['deg'] . "&long=" . $lon['deg'] ;
	$ctx = stream_context_create(array('http' => array('timeout' => 5))); 
	$region = @explode ( "\t" , file_get_contents ( $url , 0 , $ctx ) ) ;
	if ( count ( $region ) > 2 ) {
		array_shift ( $region ) ;
		array_shift ( $region ) ;
		if ( count ( $region ) > 0 ) {
			$region_name = strtoupper ( array_shift ( $region ) ) ;
			if ( $region_name != "" ) $md->p->pieces[] = "region:" . $region_name ;
		}
	}
}
$region_name = array_shift ( explode ( '-' , $region_name ) ) ;
$region_name = array_shift ( explode ( '_' , $region_name ) ) ;

# Read template
#$pagename = get_request ( "altpage" , "Template:GeoTemplate" ) ;
$pagename = "Template:GeoTemplate" ;

if ( $globe != '' && $globe != 'earth' ) {
	$pagename .= '/' . str_replace ( "&", "%26", $globe ) ;
}

# Allow testing with &sandbox=1
if ( get_request ( 'sandbox', '' ) ) {
	$pagename .= "/sandbox";
}

if ( ! get_request ( 'project' , '' ) ) {
	$request_url = "http://{$lang}.wikipedia.org/w/index.php?title=$pagename&useskin=monobook" . strftime ( "&nocache=%Y%m%d%H" ) ;
} else {
	$request_url = "http://meta.wikimedia.org/w/index.php?title=$pagename/" . get_request ( 'project' , '' ) . "&useskin=monobook" . strftime ( "&nocache=%Y%m%d%H" ) ;
	$lang = 'meta' ; 
}

$page =  @get_html ( $request_url ) ;

if ( $page === false || $page == '' ) {
	# fall back to to English Wikipedia
    $page = @get_html ( "http://en.wikipedia.org/w/index.php?title=$pagename&uselang=$lang&useskin=monobook" . strftime ( "&nocache=%Y%m%d%H" ) ) ;
}

if ( $page === false || $page == '' ) {
	header("HTTP/1.1 502 Bad Gateway");
	print "<!DOCTYPE html>" ;
	print "<html><head><title>502 Bad Gateway</title></head><body>" ;
	print "Failed to open <a href=\"{$request_url}\">{$request_url}</a>." ;
	print "</body></html>" ;
	exit ;
}

$page = str_replace ( ' href="/w' , " href=\"//{$lang}.wikipedia.org/w" , $page ) ;
$page = str_replace ( ' role="navigation"' , '' , $page ) ;
$page = str_replace ( ' class="portlet"' , '' , $page ) ;
$actions = str_replace ( 'id="p-cactions"', '', get_div_section ( $page, "p-cactions" ) ) ;
$languages = preg_replace_callback ( '/ href="(https*:)\/\/([a-z\-]+){0,1}\.wikipedia\.org\/wiki\/[^"]*/', create_function(
	'$match',
	'global $theparams, $r_pagename; return " href=\"" . make_link ( $match[2] , $theparams , $r_pagename );'
), get_div_section ( $page, "p-lang" ) ) ;

# Remove edit links
do {
	$op = $page ;
	$p = explode ( '<span class="editsection"' , $page , 2 ) ;
	if ( count ( $p ) == 1 ) continue ;
	$page = array_shift ( $p ) ;
	$p = explode ( '</span>' , array_pop ( $p ) , 2 ) ;
	$page .= array_pop ( $p ) ;
} while ( $op != $page ) ;


# Build the page
if ( strpos ( $page , '<!-- start content -->' ) ) {
	$page = array_pop ( explode ( '<!-- start content -->' , $page , 2 ) ) ;
	$page = array_shift ( explode ( '<!-- end content -->' , $page , 2 ) ) ;
} else {
	$page = array_pop ( explode ( '<!-- bodytext -->' , $page , 2 ) ) ;
	$page = array_shift ( explode ( '<!-- /bodytext -->' , $page , 2 ) ) ;
} 
# XXX There should be failsafe branch here
$md->thetext = $page ;
$page = $md->build_output () ;

# Ugly hacks
$page = str_replace ( '{nztmeasting}' , '0' , $page ) ;
$page = str_replace ( '{nztmnorthing}' , '0' , $page ) ;

# Localized services
$locmaps = get_div_section ( $page, 'GEOTEMPLATE-'.$region_name );
$locinsert =  get_div_section ( $page, 'GEOTEMPLATE-LOCAL' );
if($locmaps && $locinsert){
	$page = str_replace ( $locmaps, '', $page ) ;
	$page = str_replace ( $locinsert, $locmaps, $page ) ;
	$page = str_replace ( get_div_section ( $page, 'GEOTEMPLATE-REGIONS' ), '', $page ) ;
}

# FIXME better titles
$mytitle = "GeoHack" ;
if ( $r_title ) $mytitle = $mytitle." - ".$r_title ;
elseif ( $r_pagename ) $mytitle = $mytitle." - ".str_replace('_', ' ', $r_pagename ) ;
elseif ($lat && $lon ) $mytitle = $mytitle." (".$lat['deg']."; ".$lon['deg'].")" ;

# Headers
header('Cache-Control: max-age=86400'); # expire after 24 hours

# Output
print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>'.$mytitle.'</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex" />
<link rel="shortcut icon" href="/geohack/siteicon.png" />
<link rel="stylesheet" type="text/css" media="screen" href="./main.css" />
<script type="text/javascript" src="//'.$lang.'.wikipedia.org/w/index.php?title=MediaWiki:GeoHack.js&amp;action=raw&amp;ctype=text/javascript"></script>
</head>
<body class="mediawiki skin-modern">
<div id="mw_header"><h1 id="firstHeading">'.$mytitle.'</h1></div>

	<div id="mw_main" style="margin-top:2em;">

<div id="mw_contentwrapper"><div id="mw_content">' ;


if ( $nlzoom ) {
	print '
<div class="mw-topboxes">
	<div class="zoom_error usermessage" style="background:#c00; color:white;">
Waarschuwing:
 op deze pagina word de verouderde parameter "zoom" gebruikt, in plaats 
 daarvan zou "scale" of "dim" gebruikt moeten worden
	 </div>
 </div>' ;
}

print $page ;

print '
</div></div>

<div id="mw_portlets">';

$logo = array( # Away from the sun
	'sun'	=> '//upload.wikimedia.org/wikipedia/commons/thumb/a/a4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100801.jpg/150px-The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100801.jpg',
	'mercury' => '//upload.wikimedia.org/wikipedia/commons/thumb/3/30/Mercury_in_color_-_Prockter07_centered.jpg/150px-Mercury_in_color_-_Prockter07_centered.jpg',
	'venus'	=> '//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Venus-real_color.jpg/150px-Venus-real_color.jpg',
	''	=> '//upload.wikimedia.org/wikipedia/commons/thumb/9/97/The_Earth_seen_from_Apollo_17.jpg/150px-The_Earth_seen_from_Apollo_17.jpg',
	'earth'	=> '//upload.wikimedia.org/wikipedia/commons/thumb/9/97/The_Earth_seen_from_Apollo_17.jpg/150px-The_Earth_seen_from_Apollo_17.jpg',
	'moon'	=> '//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Full_Moon_Luc_Viatour.jpg/150px-Full_Moon_Luc_Viatour.jpg',
	# Mars and its moons
	'mars'	=> '//upload.wikimedia.org/wikipedia/commons/thumb/7/76/Mars_Hubble.jpg/150px-Mars_Hubble.jpg',
	'phobos' => '//upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Phobos_colour_2008.jpg/150px-Phobos_colour_2008.jpg',
	'deimos' => '//upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Deimos-MRO.jpg/150px-Deimos-MRO.jpg',
	# Asteroid belt
	'ceres'	   => '//upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Ceres_RC2_Bright_Spot.jpg/150px-Ceres_RC2_Bright_Spot.jpg',
	'vesta'    => '://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Vesta_in_natural_color.jpg/150px-Vesta_in_natural_color.jpg',
	# Jupiter and its moons
	'jupiter'  => '//upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Jupiter_by_Cassini-Huygens.jpg/150px-Jupiter_by_Cassini-Huygens.jpg',
	'ganymede' => '//upload.wikimedia.org/wikipedia/commons/thumb/9/93/Moon_Ganymede_by_NOAA_-_cropped.jpg/150px-Moon_Ganymede_by_NOAA_-_cropped.jpg',
	'callisto' => '//upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Callisto.jpg/150px-Callisto.jpg',
	'io'       => '//upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Io_highest_resolution_true_color.jpg/150px-Io_highest_resolution_true_color.jpg',
	'europa'   => '//upload.wikimedia.org/wikipedia/commons/thumb/5/54/Europa-moon.jpg/150px-Europa-moon.jpg',
	'amalthea' => '//upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Amalthea_PIA01076.jpg/150px-Amalthea_PIA01076.jpg',
	'thebe'    => '//upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Thebe.jpg/150px-Thebe.jpg',
	# Saturn and its moons
	'saturn'   => '//upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Saturn_during_Equinox.jpg/200px-Saturn_during_Equinox.jpg',
	'mimas'	   => '//upload.wikimedia.org/wikipedia/commons/thumb/d/da/Mimas_moon.jpg/150px-Mimas_moon.jpg',
	'enceladus'=> '//upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Enceladusstripes_cassini.jpg/150px-Enceladusstripes_cassini.jpg',
	'tethys'   => '//upload.wikimedia.org/wikipedia/commons/thumb/0/01/PIA07738_Tethys_mosaic_contrast-enhanced.jpg/150px-PIA07738_Tethys_mosaic_contrast-enhanced.jpg',
	'dione'    => '//upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Dione3_cassini_big.jpg/150px-Dione3_cassini_big.jpg',
	'rhea'     => '//upload.wikimedia.org/wikipedia/commons/thumb/a/ab/PIA07763_Rhea_full_globe5.jpg/150px-PIA07763_Rhea_full_globe5.jpg',
	'titan'    => '//upload.wikimedia.org/wikipedia/commons/thumb/8/84/Titan_in_natural_color_Cassini.jpg/150px-Titan_in_natural_color_Cassini.jpg',
	'hyperion' => '//upload.wikimedia.org/wikipedia/commons/thumb/9/94/Hyperion_true.jpg/150px-Hyperion_true.jpg',
	'iapetus'  => '//upload.wikimedia.org/wikipedia/commons/thumb/4/42/Iapetus_Roncevaux.jpg/150px-Iapetus_Roncevaux.jpg',
	'phoebe'   => '//upload.wikimedia.org/wikipedia/commons/thumb/3/32/Phoebe_cassini.jpg/150px-Phoebe_cassini.jpg',
	'janus'    => '//upload.wikimedia.org/wikipedia/commons/thumb/6/62/PIA12714_Janus_crop.jpg/150px-PIA12714_Janus_crop.jpg',
	'epimetheus'=>'//upload.wikimedia.org/wikipedia/commons/thumb/2/24/PIA09813_Epimetheus_S._polar_region.jpg/150px-PIA09813_Epimetheus_S._polar_region.jpg',
	# Uranus and its moons
	'uranus'   => '//upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Uranus2.jpg/150px-Uranus2.jpg',
	'ariel'    => '//upload.wikimedia.org/wikipedia/commons/thumb/5/59/Ariel_%28moon%29.jpg/140px-Ariel_%28moon%29.jpg',
	'umbriel'  => '//upload.wikimedia.org/wikipedia/commons/thumb/5/50/Umbriel_%28moon%29.jpg/150px-Umbriel_%28moon%29.jpg',
	'titania'  => '//upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Titania_%28moon%29_color_cropped.jpg/135px-Titania_%28moon%29_color_cropped.jpg',
	'oberon'   => '//upload.wikimedia.org/wikipedia/commons/thumb/0/09/Voyager_2_picture_of_Oberon.jpg/150px-Voyager_2_picture_of_Oberon.jpg',
	'miranda'  => '//upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Miranda_-_January_24_1986_%2840196276021%29.jpg/150px-Miranda_-_January_24_1986_%2840196276021%29.jpg',
	'puck'     => '//upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Puck.png/150px-Puck.png',
	# Neptune and its moons
	'neptune'  => '//upload.wikimedia.org/wikipedia/commons/thumb/0/06/Neptune.jpg/150px-Neptune.jpg',
	'triton'   => '//upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Triton_moon_mosaic_Voyager_2_%28large%29.jpg/150px-Triton_moon_mosaic_Voyager_2_%28large%29.jpg',
	'nereid'   => '//upload.wikimedia.org/wikipedia/commons/b/b0/Nereid-Voyager2.jpg',
	'proteus'  => '//upload.wikimedia.org/wikipedia/commons/thumb/8/83/Proteus_%28Voyager_2%29.jpg/150px-Proteus_%28Voyager_2%29.jpg',
	# Pluto and its moons
	'pluto'    => '//upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Pluto-01_Stern_03_Pluto_Color_TXT.jpg/150px-Pluto-01_Stern_03_Pluto_Color_TXT.jpg',
	'charon'   => '//upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Pluto-01_Stern_03_Pluto_Color_TXT.jpg/150px-Pluto-01_Stern_03_Pluto_Color_TXT.jpg',
	# Special
	'osm'	=> '//wiki.openstreetmap.org/Wiki.png');
print '
<div class="portlet">
<div style="background:#000 url('.(isset($logo[$globe])?$logo[$globe]:"''").') center no-repeat; height:150px;"></div>
</div>';

print $actions;

print '
<!-- languages --> 
	';
print $languages;

print '
<div class="portlet">
	<h5>GeoHack</h5>
	<div class="pBody">
		<ul>
			<li><a href="https://www.mediawiki.org/wiki/GeoHack">Documentation</a></li>
<!--			<li><a href="https://bitbucket.org/abbe98/geohack/issues">Bug tracker</a></li>
			<li><a href="https://bitbucket.org/magnusmanske/geohack">Source Code</a>)</li>-->
		</ul>
		<p style="text-align:center;"><a href="//tools.wmflabs.org/"><img border="0" alt="Powered by WMF Labs" src="//upload.wikimedia.org/wikipedia/commons/thumb/1/11/Wikimedia_labs_logo_with_text.svg/103px-Wikimedia_labs_logo_with_text.svg.png" /></a></p>
	</div>
</div>
	<!-- actions --> 
	'; # //wikitech.wikimedia.org/w/images/c/cf/Labslogo_thumb.png
print '
</div><!-- mw_portlets --> 

</div>
</body>
</html>
' ;

ob_end_flush(); # Send to client

?>